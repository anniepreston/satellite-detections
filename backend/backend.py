# python imports
import os
from pdb import set_trace as bp

# pip imports
import falcon
import psycopg2
import postgis

# project imports
from resources import FireRadiativePowerResource, FireDetectionResource, FireListResource, DetectionInterpolationResource, FireInterpolationResource, FirePerimetersResource, TargetPerimeterResource, DetectionAndRadiativePowerResource

# not super relevant
scriptPath = os.path.dirname(os.path.realpath(__file__))

# set up our PostGIS connection
psycopgConnectionString = "host=marlin.cs.ucdavis.edu user=docker port=25432 dbname=gis password=emergencyPostGIS"
pgdb = psycopg2.connect(psycopgConnectionString)
postgis.psycopg.register(pgdb)

app = application = falcon.API()

app.add_route('/fire/radiative_power', FireRadiativePowerResource(pgdb))
app.add_route('/fire/detection', FireDetectionResource(pgdb))
app.add_route('/fire/list', FireListResource(pgdb))
app.add_route('/fire/detectionInterpolation', DetectionInterpolationResource(pgdb))
app.add_route('/fire/fireInterpolation', FireInterpolationResource(pgdb))
app.add_route('/fire/firePerimeters', FirePerimetersResource(pgdb))
app.add_route('/fire/targetPerimeter', TargetPerimeterResource(pgdb))
app.add_route('/fire/detectionsAndRadiativePower', DetectionAndRadiativePowerResource(pgdb))

# static file service (ideally we'd use something like nginx here but for ease of deployment this will suffice)
# serve up the index.html file
#app.add_route("/", StaticResource())

# serve up the static content resources
#app.add_route('/{filename}', StaticContentResource())

# this is sort of hacky... server script *has* to be ran from the ./backend directory
# on Mac, use this:
staticPath = '/'.join(__file__.split('/')[0:-2]) + '/frontend/'
# on Windows, use this:
# staticPath = '\\'.join(__file__.split('\\')[0:-2]) + "\\frontend\\"
# static file service
app.add_static_route('/', staticPath)
