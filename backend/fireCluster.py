import numpy as np 
import time

from sklearn.cluster import DBSCAN
resolution = 6 # distance threshold for DBSCAN, in km

#custom distance metric for DBSCAN ([lon, lat] --> dist in km)
#converted to python by me, from http://www.movable-type.co.uk/scripts/latlong.html :
def latLonToKm(p,q):
	lat1 = p[1]
	lat2 = q[1]
	lon1 = p[0]
	lon2 = q[0]
	R = 6371 # ~earth's radius in km
	dLat = (lat2-lat1)*np.pi/180.
	dLon = (lon2-lon1)*np.pi/180.
	a = np.sin(dLat/2.) * np.sin(dLat/2.) + np.cos(lat1 * (np.pi/180)) * np.cos(lat2 * (np.pi/180.)) * np.sin(dLon/2) * np.sin(dLon/2.)
	c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
	d = R * c
	return d

def clusterDetections(frp, detections):
	data = frp + detections
	start = time.time()
	centroids = [[item['centroid']['coordinates'][0], item['centroid']['coordinates'][1]] for item in data]
	db = DBSCAN(eps=resolution, min_samples=2, metric=latLonToKm).fit(centroids) #NOTE: there are more params allowed here to play with
	end = time.time()
	print("DBScan time: ", end-start)
	start = time.time()
	labels = db.labels_
	clusters = []
	n_clusters = 1+np.amax(labels)
	print("n_clusters: ", n_clusters)
	for i in range(n_clusters):
		clusters.append([])
	for i in range(len(labels)):
		clusters[labels[i]].append(i)
	bounds = getRectBounds(centroids, clusters)
	totalBounds = getTotalBounds(bounds)
	end = time.time()
	print("get bounds time: ", end-start)

	clusterObject = {
		'indices': clusters,
		'bounds': bounds,
		'totalBounds': totalBounds
	}

	return clusterObject

def getRectBounds(centroids, clusters):
	bounds = []
	for i in range(len(clusters)):
		lats = []
		lons = []
		for j in range(len(clusters[i])):
			lons.append(centroids[clusters[i][j]][0])
			lats.append(centroids[clusters[i][j]][1])
		lonMin = np.amin(lons)
		lonMax = np.amax(lons)
		latMin = np.amin(lats)
		latMax = np.amax(lats)
		bounds.append({'lonMin': lonMin, 'lonMax': lonMax, 'latMin': latMin, 'latMax': latMax})

	return expandBounds(resolution, bounds)

def getTotalBounds(bounds):
	lonMin = float('inf')
	lonMax = float('-inf')
	latMin = float('inf')
	latMax = float('-inf')
	for b in bounds:
		if b['lonMin'] < lonMin: 
			lonMin = b['lonMin']
		if b['lonMax'] > lonMax:
			lonMax = b['lonMax']
		if b['latMin'] < latMin:
			latMin = b['latMin']
		if b['latMax'] > latMax:
			latMax = b['latMax']
	return {'lonMin': lonMin, 'lonMax': lonMax, 'latMin': latMin, 'latMax': latMax}

def expandBounds(distance, bounds):
	angle = 45*np.pi/180.
	expanded_bounds = []
	for b in bounds:
		east_disp = distance * 1000 * np.cos(angle) / 111111.
		north_disp = distance * 1000 * np.sin(angle) / np.cos(b['latMin'] * np.pi/180.) / 111111.
		expanded_bounds.append({
			'lonMin': b['lonMin']-east_disp,
			'lonMax': b['lonMax']+east_disp,
			'latMin': b['latMin']-north_disp,
			'latMax': b['latMax']+north_disp
		})
	return expanded_bounds