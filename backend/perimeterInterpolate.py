##############################################
# interpolate & predict next fire perimeters #
# based on satellite detections              #
# (based on Max's code)                      #
##############################################

from pdb import set_trace as bp
import json
import psycopg2.extras
from sklearn.neighbors.kde import KernelDensity
import numpy as np
from matplotlib.path import Path
from pprint import pprint
from multiprocessing import Process, Queue
import os

import math

def interpolatePerimeter(perimeter, frp, detections, centroid, centroidDistance, nGrid = 1000, bandwidth = 0.004, perimeterMultiplier = 1.25):

    # TODO filter frp points & det points (by confidence?) and combine them into 1 nparray 
    frpPoints = np.array([ tuple([d['centroid']['coordinates'][0], d['centroid']['coordinates'][1], d['confidence'], d['instantaneous_temperature']]) for d in frp ])
    detPoints = np.array([ tuple([d['centroid']['coordinates'][0], d['centroid']['coordinates'][1], d['confidence'], d['power']]) for d in detections ])

    perimeterGeometries = np.array([np.array(geometry[0]) for geometry in perimeter['perimeter']['coordinates'] ])

    #gridBB = [[lat0, long0], [lat1, long1]]
    lat = centroid['coordinates'][1]
    long = centroid['coordinates'][0]

    # https://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    offset = (centroidDistance) / 111111.1

    gridBB = [[lat - offset, long - offset], [lat + offset, long + offset]]

    points = None

    if len(frpPoints) > 0 and len(detPoints) > 0:
        points = np.concatenate([frpPoints, detPoints])
    elif len(frpPoints) > 0 and len(detPoints) == 0:
        points = frpPoints
    elif len(detPoints) > 0 and len(frpPoints) == 0:
        points = detPoints
    else:
        return {}

    for d in detections:
        print(d)
        print(d['sensor'])

    runGpuAccelerated = True
    runWeightedKde = False

    kdeResult = runTieredKDE(perimeterGeometries, points, gridBB, nGrid, bandwidth = bandwidth, filterByPerimeterGeometry=False, maskByPerimeterGeometry = False)
    return kdeResult

def tieredPointsMask(values, tier_num):
    # points :  np.array([ tuple([d['centroid']['coordinates'][0], d['centroid']['coordinates'][1], d['confidence'], d['power']]) for d in detections ])
    # based on d['confidence'] -- (0, 100)
    # split equally into tier_num tiers
    # values: confidence
    step = 100. / tier_num
    tierRange = np.arange(0, 100, step)
    masks = []

    # replace string values w/ uncertainty values (these are fairly arbitrary) but close to what sources say they should be
    replacementMap = {
        'Processed fire pixel; available sub-pixel estimates of instantaneous fire size and temperature': 95,
        'Saturated fire pixel': 30,
        'High possibility fire pixel': 70,
        'Medium possibility fire pixel': 50,
        'Cloud-contaminated fire pixel': 20
    }

    for key in replacementMap:
        values[np.where(values == key)] = replacementMap[key]

    # some confidence values were still being stored as numerical strings (e.g. '85') so just coerce everything to an int
    values = values.astype(int) 

    for t in tierRange:
        masks.append(np.logical_and(values > t, values <= (t + step) ))

    return masks

def runTieredKDE(geometries, points, gridBoundingBox, nGrid = 300, bandwidth = 0.01, filterByPerimeterGeometry = True, maskByPerimeterGeometry = True):
    #pointOrder = ['lat', 'long', 'confidence', 'intensity']

    x = np.linspace(gridBoundingBox[0][1], gridBoundingBox[1][1], nGrid)
    y = np.linspace(gridBoundingBox[0][0], gridBoundingBox[1][0], nGrid)

    X, Y = np.meshgrid(x, y)

    kde = KernelDensity(bandwidth=bandwidth, metric='haversine', kernel='gaussian', algorithm='ball_tree')

    tierNum = 5
    tierMasks = tieredPointsMask(points[:, 2], tierNum)

    kdeInputPoints = np.array(points[:, :2], dtype='float64')
    ## mask geometries for input points

    geometryPaths = []
    # mask the geometries 
    for geometry in geometries:
        vertices = geometry[:, :2]
        path = Path(vertices=vertices)
        geometryPaths.append(path)

    xy = np.vstack([X.ravel(), Y.ravel()]).T
        
    def getKdeResult(inputPoints):
        filteredPoints = inputPoints
        
        if filterByPerimeterGeometry is True:
            inputGeometryMask = np.ones(tuple([inputPoints.shape[0]]))

            for path in geometryPaths:
                thisMask = np.logical_not(path.contains_points(points=inputPoints))
                inputGeometryMask = np.logical_and(thisMask, inputGeometryMask) 

            filteredPoints = inputPoints[inputGeometryMask]


        if len(filteredPoints) == 0:
            return None

        kde.fit(filteredPoints)
        result = np.exp(kde.score_samples(xy))
        return result

    def getWeight(t, tier_num):
        ## very simple weighting 
        return 1. / tier_num * t

    kdeResult = np.zeros(xy.shape[0])
    for i, tm in enumerate(tierMasks):
        if not np.any(tm):
            continue

        result = getKdeResult(kdeInputPoints[tm])
        t = i + 1

        if result is None: 
            continue

        ##  weighting and summation
        kdeResult += result * getWeight(t, tierNum)

    geometryMask = np.zeros(tuple([xy.shape[0]]))

    # mask the geometries 
    for path in geometryPaths:
        thisMask = path.contains_points(points=xy)
        geometryMask = np.logical_or(thisMask, geometryMask)

    # normalize the KDE result
    kdeResult = (kdeResult - np.min(kdeResult)) / (np.max(kdeResult) - np.min(kdeResult))

    # mask the KDE result with the geometries, to ensure that geometries are considered in the model ("ground truths")
    if maskByPerimeterGeometry is True:
        kdeResult[geometryMask] = 0.0

    return {
        'kde_result_coordinates': xy.tolist(),
        'kde_result': kdeResult.tolist(),
        'range': [np.min(kdeResult), np.max(kdeResult)],
        'bounding_box': gridBoundingBox,
        'n_grid': nGrid
    }