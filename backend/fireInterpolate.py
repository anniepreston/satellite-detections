import numpy as np
import time
from haversine import haversine

bounds = {
  	'lonMin': -124.4, 
  	'latMin': 32, 
    'lonMax': -113, 
    'latMax': 42.5
}
resolution = { 
    'w': 200,
    'h': 200
}

def interpolatePoints(frp, detections, clusters):
	data = frp + detections
	start = time.time()
	lonStep = (clusters['totalBounds']['lonMax']-clusters['totalBounds']['lonMin'])/resolution['w']
	latStep = (clusters['totalBounds']['latMax']-clusters['totalBounds']['latMin'])/resolution['h']
	positions = []
	values = []
	for i in range(resolution['h']):
		for j in range(resolution['w']):
			position = { 'lon': clusters['totalBounds']['lonMin'] + j * lonStep, 'lat': clusters['totalBounds']['latMax'] - i * latStep }
			positions.append(position)
			value = interpolateValues(position, data, clusters)
			values.append(value)

	print("done")
	arraySize = (resolution['h']-1)*(resolution['w']*4 + 4)

	gridPoints = np.empty(arraySize)
	gridValues = np.empty(arraySize*2)

	# this could be less gross.
	"""
	idx = 0
	for i in range(resolution['h']-1):
		for j in range(resolution['w']):
			gridPoints[idx] = positions[i][j]['lon']
			gridPoints[idx+1] = positions[i][j]['lat']
			gridPoints[idx+2] = positions[i+1][j]['lon']
			gridPoints[idx+3] = positions[i+1][j]['lat']

			gridValues[2*idx] = values[i][j]['v']
			gridValues[2*idx+1] = values[i][j]['time']
			gridValues[2*idx+2] = values[i][j]['confidence']
			gridValues[2*idx+3] = values[i][j]['opacity']

			gridValues[2*idx+4] = values[i+1][j]['v']
			gridValues[2*idx+5] = values[i+1][j]['time']
			gridValues[2*idx+6] = values[i+1][j]['confidence']
			gridValues[2*idx+7] = values[i+1][j]['opacity']

			if j == resolution['w'] - 1:
				gridPoints[idx+4] = positions[i+1][j]['lon']
				gridPoints[idx+5] = positions[i+1][j]['lat']
				gridPoints[idx+6] = positions[i+1][0]['lon']
				gridPoints[idx+7] = positions[i+1][0]['lat']

				gridValues[2*idx+8] = values[i+1][j]['v']
				gridValues[2*idx+9] = values[i+1][j]['time']
				gridValues[2*idx+10] = values[i+1][j]['confidence']
				gridValues[2*idx+11] = values[i+1][j]['opacity']

				gridValues[2*idx+12] = values[i+1][0]['v']
				gridValues[2*idx+13] = values[i+1][0]['time']
				gridValues[2*idx+14] = values[i+1][0]['confidence']
				gridValues[2*idx+15] = values[i+1][0]['opacity']

				idx += 8
			else:
				idx += 4
	end = time.time()
	print("interpolation time: ", end-start)
	"""
	return {'points': positions, 'values': values}

def interpolateValues(point, data, clusters):
	values = {}
	lon = point['lon']
	lat = point['lat']
	cluster_indices = []
	nClusters = len(clusters['indices'])
	for j in range(len(clusters['bounds'])):
		if lon > clusters['bounds'][j]['lonMin'] and lon < clusters['bounds'][j]['lonMax'] and lat > clusters['bounds'][j]['latMin'] and lat < clusters['bounds'][j]['latMax']:
			cluster_indices.append(j)
	if len(cluster_indices) > 0:
		dists = []
		ids = []
		for c in range(len(cluster_indices)):
			cluster = cluster_indices[c]
			for j in range(len(clusters['indices'][cluster])):
				id = clusters['indices'][cluster][j]
				ids.append(id)
				dists.append(haversine([lat, lon], [data[id]['centroid']['coordinates'][1], data[id]['centroid']['coordinates'][0]]))
		vals = getWeightedValues(nClusters, ids, dists, data)
		values['v'] = vals['v']
		values['time'] = 1.0#time
		values['confidence'] = vals['confidence']
		values['opacity'] = vals['opacity']
	else:
		values['v'] = 0
		values['time'] = 0
		values['confidence'] = 0
		values['opacity'] = 0
			
	return values

def getScaledFRP(value):
	if (value < 400):
		return value/400.
	else:
		return 1.0

def getScaledTemp(value):
	if value < 1000 and value > 400: #FIXME!
		return (value-400)/(1000-400)
	elif value > 1000:
		return 1.0
	else:
		return 0.0

def getScaledConfidenceFrp(c):
	return float(c)/100.

def getScaledConfidenceDetect(c):
	categories = ["Medium possibility fire pixel", 'High possibility fire pixel', 'Processed fire pixel; available sub-pixel estimates of instantaneous fire size and temperature', "Saturated fire pixel",'Cloud-contaminated fire pixel']
	ratings = [.7, .95, 1.0, .5, .1]
	idx = categories.index(c)
	if idx == -1:
		return 0
	else:
		return ratings[idx]

def getScaledTime(styleUrl):
	styleUrls = ['# 00_to_06hr_fire ', '# 06_to_12hr_fire ', '# 12_to_24hr_fire ', '# prev_6_days_fire '];
  	# 0: 1
  	# 1: 0.9
  	# 2: 0.8
  	# 3: 0.7
	return 1 - (styleUrls.index(styleUrl))*.1

def getWeightedValues(nClusters, ids, dists, data):
	p = 2.0 #polynomial degree for IDW
	max_radius = 1.8 # in km; outside of this will be opacity = 0
	k = 5 #number of nearest neighbors to average

	indices = []
	"""
	if len(ids) <= k or nClusters == 1:
		for i in range(len(ids)):
			indices.append(i)
	else:
		indices = getNSmallestElements(dists, k)
		"""
	indices = getElementsWithinDist(dists, max_radius)
	dists_sum = 0
	smallest_dist = 1000
	for i in range(len(indices)):
		n = np.power(dists[indices[i]],p) 
		if n > 0:
			dists_sum += 1.0/n
			if dists[indices[i]] < smallest_dist:
				smallest_dist = dists[indices[i]]

	vs = []
	v = 0
	time = 0
	confidence = 0.0 #start in a shrouded, dark place
	opacity = 0

	for i in range(len(indices)):
		n = np.power(dists[indices[i]], p)
		if n > 0:
			weight = 1.0/n
			if data[ids[indices[i]]]['sensor'] == 'GOES-13' or data[ids[indices[i]]]['sensor'] == 'GOES-15':
				if data[ids[indices[i]]]['instantaneous_temperature'] is None:
					detected_v = 0
				else:
					detected_v = getScaledTemp(float(data[ids[indices[i]]]['instantaneous_temperature']))
			else:
				if 'power' in data[ids[indices[i]]]:
					detected_v = getScaledFRP(float(data[ids[indices[i]]]['power']))
				else:
					detected_v = 0
			if detected_v > 0:
				v += weight * detected_v
			time += weight * 0.5
			if data[ids[indices[i]]]['sensor'] == 'GOES-13' or data[ids[indices[i]]]['sensor'] == 'GOES-15':
				confidence += weight * getScaledConfidenceDetect(data[ids[indices[i]]]['confidence'])
			else:
				confidence += weight * getScaledConfidenceFrp(data[ids[indices[i]]]['confidence'])

	if dists_sum > 0:
		v = v/dists_sum
		confidence = confidence/dists_sum
		time = time/dists_sum
		opacity = np.power((max_radius-smallest_dist),2.0)/np.power(max_radius,2.0)
		if opacity < 0:
			opacity = 0
	if np.amin(dists) > max_radius:
		opacity = 0
	if v < 0.01:
		opacity = 0
		confidence = 1
	return {
		'v': v,
		'time': time,
		'confidence': confidence,
		'opacity': opacity
	}

def getNSmallestElements(dists, n):
	current_min = 0
	indices = []
	for i in range(n):
		array_min = np.amin(dists)
		array_min_idx = -1
		for j in range(len(dists)):
			if dists[j] < array_min and dists[j] > current_min:
				array_min = dists[j]
				array_min_idx = j
			if array_min_idx == -1:
				for k in range(len(dists)):
					if dists[k] <= array_min and dists[k] >= current_min:
						array_min = dists[k]
						array_min_idx = k
		current_min = array_min
		indices.append(array_min_idx)
	return indices

def getElementsWithinDist(dists, d):
	indices = []
	for i in range(len(dists)):
		if dists[i] < d:
			indices.append(i)
	return indices

def latLonToKm(p,q):
	lat1 = p[1]
	lat2 = q[1]
	lon1 = p[0]
	lon2 = q[0]
	R = 6371 # ~earth's radius in km
	dLat = (lat2-lat1)*np.pi/180.
	dLon = (lon2-lon1)*np.pi/180.
	a = np.sin(dLat/2.) * np.sin(dLat/2.) + np.cos(lat1 * (np.pi/180)) * np.cos(lat2 * (np.pi/180.)) * np.sin(dLon/2) * np.sin(dLon/2.)
	c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
	d = R * c
	return d