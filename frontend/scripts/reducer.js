import {
  FETCH_BEGIN,
  FETCH_FAILURE,
  FETCH_FRP_SUCCESS,
  FETCH_INTERP_SUCCESS,
  FETCH_FIRE_LIST_SUCCESS,
  FETCH_FIRE_PERIMETERS_SUCCESS,
  FETCH_CURRENT_PERIMETER_SUCCESS,
  FETCH_SATELLITE_RANGE_SUCCESS,
  FETCH_TARGET_PERIMETER_SUCCESS,
  UPDATE_MODE_SUCCESS
} from './dataActions';

const initialState = {
	items: [],
	loading: false,
	error: null
};

export default function reducer(state = initialState, action){
	console.log("action: ", action);
	switch(action.type) {
		case FETCH_BEGIN:
			// Mark the state as "loading" so we can show a spinner or something
      		// Also, reset any errors
      		return {
        		...state,
        		loading: true,
        		error: null
       		};
    	case FETCH_FAILURE:
	        return {
	            ...state,
	            loading: false,
	            error: action.payload.error,
	            items: []
	        };
    	case FETCH_FRP_SUCCESS:
      		return {
      			...state,
      			loading: false,
      			items: {frp_data: action.payload.frp_data, clusters: action.payload.clusters }
      		};
    	case FETCH_INTERP_SUCCESS:
            return {
            	...state,
            	loading: false,
            	items: { points: action.payload.points, values: action.payload.values }
            };
    	case FETCH_FIRE_LIST_SUCCESS:
            return {
            	...state,
            	loading: false,
            	fires: action.payload.fires
            };
    	case FETCH_FIRE_PERIMETERS_SUCCESS:
    		return {
    			...state,
    			loading: false,
    			perimeters: action.payload.perimeters
    		};
    	case FETCH_CURRENT_PERIMETER_SUCCESS:
    		return {
    			...state,
    			loading: false,
    			currentPerimeter: action.payload.currentPerimeter
    		};
    	case FETCH_SATELLITE_RANGE_SUCCESS:
    		return {
    			...state,
    			loading: false,
            	items: { points: action.payload.points, values: action.payload.values, frp: action.payload.frp, detections: action.payload.detections }
    		}
    	case FETCH_TARGET_PERIMETER_SUCCESS:
    		return {
    			...state,
    			loading: false //FIXME!
    		}
    	case UPDATE_MODE_SUCCESS:
    		return {
    			...state,
    			mode: action.payload.mode
    		}
    	default:
      		return {
        		...state,
        		loading: false,
        		error: null
        	};
	}
}