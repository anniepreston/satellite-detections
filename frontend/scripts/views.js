/* global window,document */
import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import MapGL from 'react-map-gl';
import DeckGLOverlay from './deckgl-overlay.js';
import GoldenLayout from 'golden-layout';
import {connect} from 'react-redux';

import {xml as requestXml} from 'd3-request';

//should hide this ...
const MAPBOX_TOKEN = 'pk.eyJ1IjoiYWtwcmVzdG8iLCJhIjoiY2pjY2I2MWpwMGV3ejJ3bnR2M3N3djB1byJ9._0l5wFhIngy5RywtZMPVlw'; // eslint-disable-line

export class Primary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        ...DeckGLOverlay.defaultViewport,
        width: 1200,
        height: 1200
      },
      points: null,
      values: null,
      mapping: 'color-frp'
	  }
  }
  componentDidMount() {
    //this.props.dispatch(fetchInterpolation());
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
  }
  _resize() {
    this._onViewportChange({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  _onViewportChange(viewport) {
    this.setState({
      viewport: {...this.state.viewport, ...viewport}
    });
  }
  
  render() {
    //FIXME: not the best place for this
    const {error, loading, points, values, frp, detections, perimeters, currentPerimeter, mode} = this.props;
    const {viewport, modis_data, goes_data, texture_data, mapping} = this.state;

    //TESTING -- FIXME
    return (
      <MapGL
        {...viewport}
        id='left_map'
        //mapStyle='mapbox://styles/akpresto/cjckwij3s00ok2smtte6ok2jj'
        mapStyle='mapbox://styles/mapbox/light-v9'
        onViewportChange={this._onViewportChange.bind(this)}
        mapboxApiAccessToken={MAPBOX_TOKEN}
      >
  	  <DeckGLOverlay viewport={viewport} points={points} values={values} frp={frp} detections={detections} mapping={mapping} perimeters={perimeters} currentPerimeter={currentPerimeter} mode={mode}/>
      </MapGL>
    );
  }
}

export class UncertaintyView extends Component {
  constructor(props){
    super(props);
    this.state = {
      viewport: {
        ...DeckGLOverlay.defaultViewport,
        width: 1200,
        height: 1200
      },
      mapping: 'color-uncertainty',
      id: 'uncertainty-view'
    }
  }

  componentDidMount() {
    //this.props.dispatch(fetchFRP());
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
  }

  _resize() {
    this._onViewportChange({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  _onViewportChange(viewport) {
    this.setState({
      viewport: {...this.state.viewport, ...viewport}
    });
  }

  render() {
    const {error, loading, points, values, frp, detections, perimeters, currentPerimeter, mode} = this.props;
    const {viewport, modis_data, goes_data, texture_data, mapping} = this.state;

    return (
      <MapGL
        {...viewport}
        id='left_map'
        mapStyle='mapbox://styles/mapbox/light-v9'
        onViewportChange={this._onViewportChange.bind(this)}
        mapboxApiAccessToken={MAPBOX_TOKEN}
      >
      <DeckGLOverlay viewport={viewport} points={points} values={values} frp={frp} detections={detections} mapping={mapping} perimeters={perimeters} currentPerimeter={currentPerimeter} mode={mode}/>
      </MapGL>
    );
  }
}

const mapStateToProps = state => ({
  points: state.items.points,
  values: state.items.values,
  frp: state.items.frp,
  detections: state.items.detections,
  perimeters: state.perimeters,
  currentPerimeter: state.currentPerimeter,
  mode: state.mode,
  loading: state.loading,
  error: state.error,
});

export const PrimaryContainer = connect(mapStateToProps)(Primary);
export const UncertaintyViewContainer = connect(mapStateToProps)(UncertaintyView);