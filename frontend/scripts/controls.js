import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import * as d3 from 'd3';
import {updateMode} from './dataActions';

export class Controls extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
      		height: window.innerHeight,
      		width: window.innerWidth,
      		modeIndex: 0,
      		modes: ['raw detections', 'fused detections', 'interpolation']
      	};
    	this.createControls = this.createControls.bind(this);
    	this.updateModeIndex = this.updateModeIndex.bind(this);
  	}
  	componentDidMount() {
    	this.createControls();
  	}

  	componentDidUpdate() {
  		this.createControls();
  	}

  	createControls() {
  		const node = this.node;
  		d3.select(node).selectAll("*").remove();
  		const {width, height, modeIndex, modes} = this.state;

  		var viewModeContainer = d3.select(node).append("svg")
  			.attr("width", width)
  			.attr("height", height)
  			.attr("x", 0)
  			.attr("y", 0);

  		viewModeContainer.append("text")
  			.attr("text", "satellite mode")
  			.attr("font-family", "sans-serif")
  			.attr("font-size", "20px")
  			.attr("fill", "#c6c6c6")
  			.attr("x", 20)
  			.attr("y", 20);

  		viewModeContainer.selectAll("rect .modebox")
  			.data(modes)
  			.enter()
  			.append("rect")
  			.attr("class", "modebox")
  			.attr("width", 12)
  			.attr("height", 12)
  			.attr("x", 20)
  			.attr("y", function(d,i){ return 40+i*20; })
  			.attr("stroke", "#c6c6c6")
  			.attr("fill", "#c6c6c6")
  			.attr("fill-opacity", function(d,i){ if (i == modeIndex) return 1; else return 0;})
	        .on("click", function(d,i){
	        	console.log("clicked!");
	        	handleClick(i);
	        });
  		viewModeContainer.selectAll("rect .modetext")
  			.data(modes)
  			.enter()
  			.append("text")
  			.attr("width", 100)
  			.attr("height", 30)
  			.attr("class", "modetext")
  			.attr("transorm", function(d,i){ return "translate(20," + 40+i*20 + ")"; })
  			.attr("fill", "#c6c6c6")
	        .attr("text", function(d){ return d; })
	        .attr("font-family", "sans-serif")
	        .attr("font-size", "14px");

    	const handleClick = idx => this.updateModeIndex(idx);
  	}

  	updateModeIndex(i){
	    this.setState({modeIndex: i});
	    const {modes} = this.state;
        this.props.dispatch(updateMode(modes[i]));
  	}

  	//exclusive checkboxes for current state:
  	// - raw detections --> geoJSON layer
  	// - fused detections (mapped to same scales) --> adjustable --> geoJSON layer (with new color mappings)
  	// - interpolation: --> my custom layer
  		// IDW,
  		// KDE,
  		// kriging

  	// AND, legends?

  	render() {
        return (
          <div style={{'width': '100%', 'height': '100%'}} ref={node => this.node = node}> 
          </div>
        );
  	}
}

const mapStateToProps = state => ({
  perimeters: state.perimeters,
  loading: state.loading,
  error: state.error,
  currentPerimeter: state.currentPerimeter
});

export const ControlsContainer = connect(mapStateToProps)(Controls);