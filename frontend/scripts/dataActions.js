export const FETCH_BEGIN = 'FETCH_BEGIN';
export const FETCH_FAILURE = 'FETCH_FAILURE';

export const FETCH_FRP_SUCCESS = 'FETCH_FRP_SUCCESS';
export const FETCH_INTERP_SUCCESS = 'FETCH_INTERP_SUCCESS';
export const FETCH_FIRE_LIST_SUCCESS = 'FETCH_FIRE_LIST_SUCCESS';
export const FETCH_FIRE_PERIMETERS_SUCCESS = 'FETCH_FIRE_PERIMETERS_SUCCESS';
export const FETCH_CURRENT_PERIMETER_SUCCESS = 'FETCH_CURRENT_PERIMETER_SUCCESS';
export const FETCH_SATELLITE_RANGE_SUCCESS = 'FETCH_SATELLITE_RANGE_SUCCESS';
export const FETCH_TARGET_PERIMETER_SUCCESS = 'FETCH_TARGET_PERIMETER_SUCCESS';
export const UPDATE_MODE_SUCCESS = 'UPDATE_MODE_SUCCESS';

import { postFireList, postFireRadiativePower, postFireDetection, postFireList, postFirePerimeters, postDetectionInterpolation, postTargetPerimeter, postDetectionsAndRadiativePower } from './api.js';

//TEMP: get these from the store (?)	
var startDate = "2017-10-12 00:00:00";
var endDate = "2017-10-12 18:00:00";
var distance = '1000000'; //?
var centroid = {type: 'Point', coordinates: [-120,38]};
var name = 'Nuns'; //this should be the only hard-coded thing for my demo
var state = 'CA';

export const fetchBegin = () => ({
	type: FETCH_BEGIN
});

export const fetchFailure = error => ({
  type: FETCH_FAILURE,
  payload: { error }
});

export const fetchFrpSuccess = data => ({
	type: FETCH_FRP_SUCCESS, 
	payload: { frp_data: data.frp_data, clusters: data.clusters }
});

export const fetchInterpSuccess = data => ({
  type: FETCH_INTERP_SUCCESS,
  payload: { values: data.values, points: data.points }
});

export const fetchFireListSuccess = data => ({
  type: FETCH_FIRE_LIST_SUCCESS,
  payload: { fires: data.fires }
});

export const fetchFirePerimetersSuccess = data => ({
  type: FETCH_FIRE_PERIMETERS_SUCCESS,
  payload: { perimeters: data.perimeters }
})

export const fetchCurrentPerimeterSuccess = data => ({
  type: FETCH_CURRENT_PERIMETER_SUCCESS,
  payload: { currentPerimeter: data.currentPerimeter }
})

export const fetchSatelliteRangeSuccess = data => ({
  type: FETCH_SATELLITE_RANGE_SUCCESS,
  payload: { values: data.values, points: data.points, frp: data.frp, detections: data.detections }
});

export const fetchTargetPerimeterSuccess = data => ({
	type: FETCH_TARGET_PERIMETER_SUCCESS,
	payload: { data: data }
});

export const updateModeSuccess = data => ({
  type: UPDATE_MODE_SUCCESS,
  payload: { mode: data.mode }
});

export function fetchFRP() {
  return function (dispatch) {
    dispatch(fetchBegin());

    return postFireRadiativePower({
		'distance': distance,
		'origin_centroid': centroid,
		'start_date': startDate,
		'end_date': endDate
  	})
  	.then(function(data){
  		const clusters = getClusters(data);
  		dispatch(fetchFrpSuccess({frp_data: data, clusters: clusters}));
  	}).catch(function(error){
  		dispatch(fetchFailure(error));
  	});
  }
}

export function fetchInterpolation() {
  return function (dispatch) {
    dispatch(fetchBegin());

    return postDetectionInterpolation({
      'distance': distance,
      'origin_centroid': centroid,
      'start_date': startDate,
      'end_date': endDate
    })
    .then(function(data){
      dispatch(fetchInterpSuccess({points: data.points, values: data.values}))
    }).catch(function(error){
      dispatch(fetchFailure(error));
    })
  }
}

export function fetchFireList() {
  return function (dispatch) {
    dispatch(fetchBegin());

    return postFireList({
      'state': state
    }).then(function(data){
      dispatch(fetchFireListSuccess({fires: data}))
    }).catch(function(error){
      dispatch(fetchFailure(error));
    })
  }
}

export function fetchFirePerimeters(fire) {
  return function (dispatch) {
    dispatch(fetchBegin());

    return postFirePerimeters({
      'state': state,
      'fire_name': fire.fire_name
    }).then(function(data){
      dispatch(fetchFirePerimetersSuccess({ perimeters: data}));
    }).catch(function(error){
      dispatch(fetchFailure(error));
    })
  }
}

export function fetchCurrentPerimeter(i){
  return function (dispatch) {
    dispatch(fetchBegin());
    //fetch related satellite date
    //then, get the interpolation
    return dispatch(fetchCurrentPerimeterSuccess({currentPerimeter: i}));
  }
}

export function fetchSatelliteRange(start_date, end_date, perimeter, mode){
//NOTE: remove mode; fetch interpolation and detections all at once
//for now, interpolation is a separate mode request because it's slow

  return function (dispatch) {
    dispatch(fetchBegin());

    const perimeterMultiplier = 3.5 //FIXME? this is very case-sensitive
    const centroid = perimeter['centroid'];
    const startDate = String(start_date.getFullYear()) + '-' + formatDate(start_date.getMonth()) + '-' + formatDate(start_date.getDate()) 
      + ' ' + formatDate(start_date.getHours()) + ':' + formatDate(start_date.getMinutes()) + ':' + formatDate(start_date.getSeconds());
    const endDate = String(end_date.getFullYear()) + '-' + formatDate(end_date.getMonth()) + '-' + formatDate(end_date.getDate())
      + ' ' + formatDate(end_date.getHours()) + ':' + formatDate(end_date.getMinutes()) + ':' + formatDate(end_date.getSeconds());
    const distance = perimeterMultiplier * Math.sqrt(4046.86 * perimeter['acres']); 

    if (mode == 'raw detections' || mode == 'fused detections'){
    	return postDetectionsAndRadiativePower({
      		'distance': distance,
      		'origin_centroid': centroid,
      		'start_date': startDate,
      		'end_date': endDate
    	}).then(function(data){
      		console.log("fetched frp and detections data! ", data);
      		dispatch(fetchSatelliteRangeSuccess({points: data.points, values: data.values, frp: data.frp, detections: data.detections}));
    	}).catch(function(error){
      		dispatch(fetchFailure(error));
    	})
  	}
    else if (mode == 'interpolation'){
      //IDW case:
      return postDetectionInterpolation({
        'distance': distance,
        'origin_centroid': centroid,
        'start_date': startDate,
        'end_date': endDate
      })
      .then(function(data){
        dispatch(fetchInterpSuccess({points: data.points, values: data.values, frp: data.frp, detections: data.detections}));
      }).catch(function(error){
        dispatch(fetchFailure(error));
      })
      //KDE case:
      /*
      return postTargetPerimeter({
          'distance': distance,
          'origin_centroid': centroid,
          'start_date': startDate,
          'end_date': endDate,
          'perimeter': perimeter
      }).then(function(data){
        console.log("fetched KDE interpolation: ", data);
        var points = [];
        var values = [];
        for (var i = 0; i < data.kde_result_coordinates.length; i++){
          points.push({'lon': data.kde_result_coordinates[i][0], 'lat': data.kde_result_coordinates[i][1]});
          values.push(({'v': data.kde_result[i]}));
        }
        dispatch(fetchSatelliteRangeSuccess(({points: points, values: values, frp: data.frp, detections: data.detectiosn})));
      }).catch(function(error){
        dispatch(fetchFailure(error));
      })
      */
    }
  }
}

export function fetchTargetPerimeter(parameters){
	return function (dispatch) {
		dispatch(fetchBegin());
		console.log("fetching target perimeter: ", parameters);

		const target_date = parameters.target_date;
		var target = String(target_date.getFullYear()) + '-' + formatDate(target_date.getMonth()) + '-' + formatDate(target_date.getDate()) + '\ ' + formatDate(target_date.getHours()) + ':' + formatDate(target_date.getMinutes()) + ':' + formatDate(target_date.getSeconds());

		return postTargetPerimeter({
			'target_date': target,
			'perimeter_date': parameters.perimeter_date,
			'state': parameters.state,
			'fire_name': parameters.fire_name
		}).then(function(data){
			console.log("fetched target perimeter: ", data);
			dispatch(fetchTargetPerimeterSuccess(data));
		}).catch(function(error){
			dispatch(fetchFailure(error));
		})
	}
}

export function updateMode(mode){
    return function (dispatch) {
      return dispatch(updateModeSuccess({mode: mode}));
    }
}

function formatDate(item){
	if (item < 10){
		var formatted = '0' + String(item);
	}
	else {
		var formatted = String(item);
	}
	return formatted;
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

const getClusters = data =>  {
  const clusters = clusterDetections(data);
  return clusters;
}