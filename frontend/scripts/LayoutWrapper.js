	import GoldenLayout from 'golden-layout';
import { Provider, connect } from 'react-redux';
import { PrimaryContainer, UncertaintyViewContainer} from './views.js';
import { FireListContainer } from './fireListView.js';
import { TimelineContainer } from './timelineView.js';
import { ControlsContainer } from './controls.js';
import PropTypes from 'prop-types';
import syncMove from '@mapbox/mapbox-gl-sync-move';

class LayoutWrapper extends React.Component {
	componentDidMount() {
		const config = {
			height: 100,
			content: [{
				type: 'column',
				height: 100,
				content: [{
					type: 'row',
					height: 20,
					content: [{
						type: 'react-component',
						component: 'TimelineContainer',
						componentName: 'Timeline',
						title: 'Timeline',
						height: 20,
						width: 90
					},{
						type: 'react-component',
						component: 'ControlsContainer',
						componentName: 'Controls',
						title: 'Controls',
						height: 20,
						width: 10
					}]
				},{
					type: 'stack',
					height: 80,
					content: [{
						type: 'react-component',
						component: 'PrimaryContainer',
						componentName: 'Satellite Detections',
						title: 'Satellite Detections',
						height: 100
					}, {
						type: 'react-component',
						component: 'UncertaintyViewContainer',
						componentName: 'Uncertainty',
						title: 'Uncertainty',
						height: 100
					}, {
						type: 'react-component',
						component: 'FireListContainer',
						componentName: 'Fire List',
						title: 'Fire List',
						height: 100
					}]
				}]
			}]
		};

		function wrapComponent(Component, store) {
			class Wrapped extends React.Component {
				render() {
					return React.createElement(
						Provider,
						{ store: store },
						React.createElement(Component, this.props)
					);
				}
			}
			return Wrapped;
		};

		var layout = new GoldenLayout(config, this.layout);
		layout.registerComponent('PrimaryContainer', wrapComponent(PrimaryContainer, this.context.store));
		layout.registerComponent('UncertaintyViewContainer', wrapComponent(UncertaintyViewContainer, this.context.store));
		layout.registerComponent('FireListContainer', wrapComponent(FireListContainer, this.context.store));
		layout.registerComponent('TimelineContainer', wrapComponent(TimelineContainer, this.context.store));
		layout.registerComponent('ControlsContainer', wrapComponent(ControlsContainer, this.context.store));
		layout.init();
		/*
		console.log("mapboxgl-map s: ");
		console.log(document.getElementsByClassName('mapboxgl-map'));
		syncMove(document.getElementsByClassName('mapboxgl-map')[0], document.getElementsByClassName('mapboxgl-map')[1]);
		*/
		window.addEventListener('resize', () => {
			layout.updateSize();
		});
	}

	render() {
		return React.createElement('div', { className: 'goldenLayout', ref: input => this.layout = input });
	}
}

// ContextTypes must be defined in order to pass the redux store to exist in
// "this.context". The redux store is given to GoldenLayoutWrapper from its
// surrounding <Provider> in index.jsx.

LayoutWrapper.contextTypes = {
	store: PropTypes.object.isRequired
};

export default LayoutWrapper;