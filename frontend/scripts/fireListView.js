/* global window,document */
import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import MapGL from 'react-map-gl';
import DeckGLOverlay from './deckgl-overlay.js';
import GoldenLayout from 'golden-layout';
import {connect} from 'react-redux';
import { fetchFireList, fetchFirePerimeters, fetchInterpolation } from './dataActions';
import EventTable from './eventTable';

import {xml as requestXml} from 'd3-request';

//should hide this ...
const MAPBOX_TOKEN = 'pk.eyJ1IjoiYWtwcmVzdG8iLCJhIjoiY2pjY2I2MWpwMGV3ejJ3bnR2M3N3djB1byJ9._0l5wFhIngy5RywtZMPVlw'; // eslint-disable-line

export class FireList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'firelist': []
	  };
    this.setSelectedFire = this.setSelectedFire.bind(this);

  }
  componentDidMount() {
    this.props.dispatch(fetchFireList());
    //this.setSelectedFire(this.props['selectedFire']);
  }
  
  render() {
    //FIXME: not the best place for this
    const {error, loading, events} = this.props;

    //TESTING -- FIXME
    return (
      <div style={{'overflowY': 'scroll', 'height': '100%'}}>
        <EventTable events={events} setSelectedFire={ this.setSelectedFire }/>
      </div>
    );
  }

  setSelectedFire(selectedFire){
  		console.log("setting selected fire in fire list view: ", selectedFire);
        this.props.dispatch(fetchFirePerimeters(selectedFire));
        //this.props.dispatch(fetchInterpolation(selectedFire));
  }
}

const mapStateToProps = state => ({
  events: state.fires,
  loading: state.loading,
  error: state.error,
});

export const FireListContainer = connect(mapStateToProps)(FireList);