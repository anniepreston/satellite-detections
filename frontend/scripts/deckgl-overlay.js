import React, {Component} from 'react';
import DeckGL, { GeoJsonLayer, GridCellLayer } from 'deck.gl';
import SatelliteDetectionLayer from './satellite-detection-layer';
import UncertaintyGridLayer from './uncertainty-grid-layer';
import * as d3 from 'd3';
import * as vsup from 'vsup';

export default class DeckGLOverlay extends Component {
  static get defaultViewport() {
    return {
      longitude: -115,
      latitude: 37,
      zoom: 5.5,
      maxZoom: 20,
      pitch: 0,
      bearing: 0
    };
  }
  
  parsePerimeterData(perimeters) {
    var perimeterData = {
            'type': 'FeatureCollection',
            'features': []
    };
    var idx = 0;
    const {currentPerimeter} = this.props;
    if (typeof(currentPerimeter) !== 'undefined' && currentPerimeter !== null){
      idx = currentPerimeter;
    }
    if (typeof(perimeters) !== 'undefined' && perimeters.length > 0){
      //for (var i = 0; i < perimeters.length; i++){
        var currentPerimeterData = Object.assign({}, perimeters[idx]['perimeter']);
        currentPerimeterData['properties'] = {
            'lineColor': [79, 79, 79, 255],
            'fillColor': 'none',
            'color': [255, 0, 0, 125]
        };
        perimeterData.features.push({
            'type': 'Feature',
            'geometry': currentPerimeterData
        });
      //}
    }
    return perimeterData;
  }

  parseDetectionData(detections) {
  	var detectionData = {
  		'type': 'FeatureCollection',
  		'features': []
  	}
  	for (var i = 0; i < detections.length; i++){
      if (detections[i]['sensor'] == 'GOES-15' || detections[i]['sensor'] == 'GOES-13'){
    		var currentDetectionData = Object.assign({}, detections[i]['footprint']);
    		var valueColor = this.getTempColor(detections[i]);
    		var uncertaintyColor = this.getTempUncertaintyColor(detections[i]);
    		currentDetectionData['properties'] = {
    			'fillColor': valueColor,
     			'uncertaintyColor': uncertaintyColor
    		}
    		detectionData.features.push({
    			'type': 'Feature',
    			'geometry': currentDetectionData
    		});
      }
  	}
  	return detectionData;
  }

  parseFrpData(frp) {
  	var frpData = {
  	  	'type': 'FeatureCollection',
  	  	'features': []
  	}
  	for (var i = 0; i < frp.length; i++){
  		var currentFrpData = Object.assign({}, frp[i]['footprint']);
  		var valueColor = this.getFrpColor(frp[i]);
  		var uncertaintyColor = this.getFrpUncertaintyColor(frp[i]);
  		currentFrpData['properties'] = {
  			'fillColor': valueColor,
  			'uncertaintyColor': uncertaintyColor
   		}
  		frpData.features.push({
  			'type': 'Feature',
  			'geometry': currentFrpData
  		});
  	}
  	return frpData;
  }

  parseAndFuseData(detections, frp) {
    var data = {
      'type': 'FeatureCollection',
      'features': []
    }
    for (var i = 0; i < detections.length; i++){
      if (detections[i]['sensor'] == 'GOES-15' || detections[i]['sensor'] == 'GOES-13'){
        var currentDetectionData = Object.assign({}, detections[i]['footprint']);
        var valueColor = this.getFusedColor(detections[i], 'detection');
        var uncertaintyColor = this.getFusedUncertainty(detections[i], 'detection');
        currentDetectionData['properties'] = {
          'fillColor': valueColor,
          'uncertaintyColor': uncertaintyColor
        }
        data.features.push({
          'type': 'Feature',
          'geometry': currentDetectionData
        });
      }
    }
    for (var i = 0; i < frp.length; i++){
      var currentFrpData = Object.assign({}, frp[i]['footprint']);
      var valueColor = this.getFusedColor(frp[i], 'frp');
      var uncertaintyColor = this.getFusedUncertainty(frp[i],'frp');
      currentFrpData['properties'] = {
        'fillColor': valueColor,
        'uncertaintyColor': uncertaintyColor
      }
      data.features.push({
        'type': 'Feature',
        'geometry': currentFrpData
      });
    }
    return data;
  }

  getTempColor(detection){
  	//scale (just temp.)
  	//assign a neutral color for null
  	var t = detection['instantaneous_temperature'];
  	if (t == null){
  		return [0, 0, 0, 0];	
  	}

  	var temp_range = [400, 1000]; //CUTOFF
  	var temp_scale = d3.scaleLinear()
  		.domain(temp_range)
  		.range([0,1]);
  	var t_scaled = temp_scale(parseFloat(t));
  	var t_string = d3.interpolateBlues(t_scaled); //scale: BLUE
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(t_string);
    return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
  }

  getFrpColor(frp){
  	//green scale
  	var f = parseFloat(frp['power']);
  	var f_range = [0,400];
  	var f_scale = d3.scaleLinear()
  		.domain(f_range)
  		.range([0,1]);
  	var f_scaled = f_scale(f);
  	var f_string = d3.interpolateGreens(f_scaled); //scale: GREEN
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(f_string);
    return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
  }

  getTempUncertaintyColor(detection){
  	var c = detection['confidence']
  	//most uncertain = dark; most certain = light
  	const categories = ["Medium possibility fire pixel", 
      'High possibility fire pixel', 
      'Processed fire pixel; available sub-pixel estimates of instantaneous fire size and temperature',
      "Saturated fire pixel",
      'Cloud-contaminated fire pixel',
      ]; 
  	const ratings = [.7, .9, .95, .2, .1]; //FIXME (higher = more confident)
  	var idx = categories.indexOf(c);
  	if (idx == -1){ 
  		console.log('unfound confidence category!: ', c);
  		var color_string = d3.interpolateBrBG(0);
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
  	}
  	else {
      var scaled_idx = ratings[idx]/2.;
      var color_string = d3.interpolateBrBG(scaled_idx);
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
    }

  }

  getFrpUncertaintyColor(frp){
  	var c = frp['confidence']
  	var c_scale = d3.scaleLinear()
  		.domain([0,100])
  		.range([1,0]);
  	var c_scaled = c_scale(c);
  	var c_string = d3.interpolateGreys(c_scaled);
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(c_string);
    return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
  }

  getFusedColor(d, type){
    //FIXME: VSUP or adjacent
    const encoding = 'VSUP';//'VSUP';

    if (encoding == 'VSUP'){
      var quantization = vsup.quantization().branching(2).layers(4).valueDomain([0, 1]).uncertaintyDomain([0,1]);
      var scale = vsup.scale().quantize(quantization).range(d3.interpolateInferno);
    }

    if (type == 'detection'){
      var t = d['instantaneous_temperature'];
      if (t == null){
        return [0,0,0,0];
      }
      var temp_range = [400, 1000]; //CUTOFF?
      var temp_scale = d3.scaleLinear()
        .domain(temp_range)
        .range([0,1]);
      var t_scaled = temp_scale(parseFloat(t));
      var c_scaled = this.getDetectionUncertainty(d);
      if (encoding == 'VSUP') { var t_string = scale(1.0-t_scaled, c_scaled); }//scale: BLUE
      else { var t_string = d3.rgb(d3.interpolateInferno(1.0-t_scaled)); }
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(t_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
    }
    else if (type == 'frp'){
      var f = parseFloat(d['power']);
      var f_range = [0,400];
      var f_scale = d3.scaleLinear()
        .domain(f_range)
        .range([0,1]);
      var f_scaled = f_scale(f);
      var c_scaled = this.getFrpUncertainty(d);
      if (encoding == 'VSUP') { var f_string = scale(1.0-f_scaled, c_scaled); } //scale: GREEN
      else { var f_string = d3.rgb(d3.interpolateInferno(1.0-f_scaled)); }
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(f_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
    }
  }

  getDetectionUncertainty(d){ //0 = least confident; 1 = most confident
      var c = d['confidence']
      //most uncertain = dark; most certain = light
      const categories = ["Medium possibility fire pixel", 
        'High possibility fire pixel', 
        'Processed fire pixel; available sub-pixel estimates of instantaneous fire size and temperature',
        "Saturated fire pixel",
        'Cloud-contaminated fire pixel',
      ]; 
      const ratings = [.7, .9, 1.0, .3, .2]; //FIXME (higher = more confident)
      var idx = categories.indexOf(c);
      if (idx == -1) return 0;
      return 1.0-ratings[idx];
  }

  getFrpUncertainty(d){
      var c = d['confidence']
      var c_scale = d3.scaleLinear()
        .domain([0,100])
        .range([1,0]);
      return c_scale(c);
  }

  getFusedUncertainty(d, type){
    var scale = d3.interpolateGreys;

    if (type == 'detection'){
      var c = this.getDetectionUncertainty(d);
      var color_string = scale(c);
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
    }
    else if (type == 'frp'){
      var c = this.getFrpUncertainty(d);
      var c_string = scale(c);
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(c_string);
      return [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]), 175];
    }
  }

  parseInterpolation(points, values){
    //no uncertainty for now...
    var data = [];
    for (var i = 0; i < points.length; i++){
      var c_string = d3.rgb(d3.interpolateInferno(1.0-values[i].v));
      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(c_string);
      var opacity =values[i].opacity*160;
      /*
      var opacity = 1;
      if (values[i].v < 0.01) opacity = 0;
      if (values[i].v >= 0.01 && values[i].v < 0.3) {
        opacity = (values[i].v - 0.01)/(0.3-0.01);
      }
      */
      var color = [parseFloat(digits[2]), parseFloat(digits[3]), parseFloat(digits[4]),opacity];//opacity*170];
      data.push({
        'position': [points[i].lon, points[i].lat],
        'color': color,
        'elevation': 0
      });
    }
    return data;
  }

  render() {
    const {viewport, points, values, frp, detections, perimeters, mapping, mode} = this.props;
    var layers = [];
    /*
    if (points != null && points.length > 0 && mode == 'interpolation'){
        layers.push(new SatelliteDetectionLayer({
          id: 'detections',
          points: points,
          values: values,
          mapping: mapping
        }));
        */
        /*
        */
    if (frp != null && frp.length > 0 && detections != null && detections.length > 0 && mode == 'raw detections'){
    	const detectionData = this.parseDetectionData(detections);
    	const frpData = this.parseFrpData(frp);

    	if (mapping != 'color-uncertainty'){
	    	//detection layer:
	    	layers.push(new GeoJsonLayer({
	    		id: 'detections-layer',
	    		data: detectionData,
	    		filled: true,
	     		stroked: true,
	    		getFillColor: d => d['geometry']['properties']['fillColor'],
	    		lineWidthMinPixels: 1,
	    		extruded: false
	    	}));

	    	//frp layer:
	    	layers.push(new GeoJsonLayer({
	    		id: 'detections-layer',
	    		data: frpData,
	    		filled: true,
	    		stroked: true,
	    		getFillColor: d => d['geometry']['properties']['fillColor'],
	    		lineWidthMinPixels: 1,
	    		extruded: false
	    	}));
    	}
    	else{
	    	//detection layer:
	    	layers.push(new GeoJsonLayer({
	    		id: 'detections-layer',
	    		data: detectionData,
	    		filled: true,
	     		stroked: true,
	    		getFillColor: d => d['geometry']['properties']['uncertaintyColor'],
	    		lineWidthMinPixels: 1,
	    		extruded: false
	    	}));

	    	//frp layer:
	    	layers.push(new GeoJsonLayer({
	    		id: 'detections-layer',
	    		data: frpData,
	    		filled: true,
	    		stroked: true,
	    		getFillColor: d => d['geometry']['properties']['uncertaintyColor'],
	    		lineWidthMinPixels: 1,
	    		extruded: false
	    	}));
    	}
    }
    if (frp != null && frp.length > 0 && detections != null && detections.length > 0 && mode == 'fused detections'){
        const data = this.parseAndFuseData(detections, frp);
        if (mapping != 'color-uncertainty'){
        //detection layer:
          layers.push(new GeoJsonLayer({
            id: 'detections-layer',
            data: data,
            filled: true,
            stroked: true,
            getFillColor: d => d['geometry']['properties']['fillColor'],
            lineWidthMinPixels: 1,
            extruded: false
          }));
        }
        else {
        //detection layer:
          layers.push(new GeoJsonLayer({
            id: 'detections-layer',
            data: data,
            filled: true,
            stroked: true,
            getFillColor: d => d['geometry']['properties']['uncertaintyColor'],
            lineWidthMinPixels: 1,
            extruded: false
          }));
        }
    }
    if (perimeters != null && typeof(perimeters) !== 'undefined'){
      const perimeterData = this.parsePerimeterData(perimeters);
      layers.push(new GeoJsonLayer({
        id: 'perimeter-layer',
        data: perimeterData,
        filled: false,
        stroked: true,
        lineWidthMinPixels: 5,
        lineJointRounded: true,
        extruded: false
      })) 
    }

    if (points != null && points.length > 0 && values != null && values.length > 0 && mode == 'interpolation'){
      const data = this.parseInterpolation(points, values);
      layers.push(new GridCellLayer({
        id: 'interpolation-layer',
        data: data,
        coverage: 1
      }));
      layers.push(new UncertaintyGridLayer({
        id: 'uncertainty-grid',
        points: points,
        values: values,
        minColor: [0, 0, 0, 0],
        cellSizePixels: 50 //FIXME
      }));
    }

    if (perimeters != null && typeof(perimeters) !== 'undefined'){
      const perimeterData = this.parsePerimeterData(perimeters);
      layers.push(new GeoJsonLayer({
        id: 'perimeter-layer',
        data: perimeterData,
        filled: false,
        stroked: true,
        lineWidthMinPixels: 5,
        lineJointRounded: true,
        extruded: false
      })) 
    }
    return <DeckGL {...viewport} layers={layers} />;
  }
}