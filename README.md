# README #

uncertainty-aware, multi-(spatial, temporal)-resolution interpolation of raw satellite detection data
 
~ ~ ~
 
React+Redux+DeckGL

* * * *

cd backend

gunicorn backend:app --reload

~ ~ ~

cd frontend

npm install

npm run build

~ ~ ~

localhost:8000/index.html

